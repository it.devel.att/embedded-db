
env:
	cp .env.example .env

run-bolt:
	go run cmd/bolt/main.go

run-badger:
	go run cmd/badger/main.go
