package servers

type Server interface {
	Run()
	Stop() error
}
