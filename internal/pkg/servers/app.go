package servers

import (
	"context"
	"net/http"

	"go.uber.org/zap"

	"embedded-db/internal/pkg/configs"
)

type BaseServer struct {
	log    *zap.Logger
	cfg    configs.Server
	server *http.Server
}

func NewServer(log *zap.Logger, cfg configs.Server, h http.Handler) *BaseServer {
	s := &BaseServer{log: log, cfg: cfg}
	s.server = &http.Server{Addr: cfg.Port, Handler: h}
	return s
}

func (s *BaseServer) Run() {
	go func() {
		if err := s.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			s.log.Fatal("", zap.Error(err))
		}
	}()
}

func (s *BaseServer) Stop() error {
	return s.server.Shutdown(context.Background())
}
