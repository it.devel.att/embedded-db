package handlers

import (
	"math/rand"
	"net/http"
	"strconv"

	"go.uber.org/zap"

	"embedded-db/internal/pkg/repositories"
)

type UserHandler struct {
	log  *zap.Logger
	repo repositories.UserRepo
	http.Handler
}

func NewUserHandler(
	log *zap.Logger,
	repo repositories.UserRepo,
) *UserHandler {
	mux := http.NewServeMux()
	handler := &UserHandler{log: log, repo: repo, Handler: mux}
	mux.HandleFunc("/user", handler.GetUser)
	return handler
}

func (h *UserHandler) GetUser(w http.ResponseWriter, r *http.Request) {
	// For tests
	randUserID := rand.Intn(1_000_000)
	_, err := h.repo.GetUser(strconv.Itoa(randUserID))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
}
