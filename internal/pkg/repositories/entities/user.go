package entities

type User struct {
	ID   string  `json:"id"`
	Rate float64 `json:"rate"`
}
