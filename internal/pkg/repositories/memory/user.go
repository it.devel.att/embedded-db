package memory

import (
	"errors"
	"sync"
	"time"

	"github.com/benbjohnson/clock"

	"embedded-db/internal/pkg/metrics"
	"embedded-db/internal/pkg/repositories"
	"embedded-db/internal/pkg/repositories/entities"
)

const (
	source  = "memory_map"
	service = "user_repo"
)

type UserRepo struct {
	client sync.Map
	time   clock.Clock
	red    metrics.REDMetrics
	bucket []byte
}

var _ repositories.UserRepo = (*UserRepo)(nil)

type Option func(repo *UserRepo)

func WithREDMetric(redMetrics metrics.REDMetrics) Option {
	return func(repo *UserRepo) {
		repo.red = redMetrics
	}
}

func NewUserRepo(opts ...Option) (*UserRepo, error) {
	ur := &UserRepo{
		client: sync.Map{},
		bucket: []byte("users"),
		time:   clock.New(),
	}

	for _, opt := range opts {
		opt(ur)
	}
	return ur, nil
}

func (u *UserRepo) GetUser(id string) (user entities.User, err error) {
	startTime, methodName := u.time.Now(), "get_user"
	defer u.updateMetrics(methodName, startTime, err)

	result, ok := u.client.Load(id)
	if !ok {
		return user, errors.New("not found")
	}
	user, ok = result.(entities.User)
	if !ok {
		return user, errors.New("error converting")
	}
	return user, nil
}

func (u *UserRepo) SaveBatch(users []entities.User) (err error) {
	startTime, methodName := u.time.Now(), "save_batch"
	defer u.updateMetrics(methodName, startTime, err)

	for i := range users {
		u.client.Store(users[i].ID, users[i])
	}
	return nil
}

func (u *UserRepo) updateMetrics(method string, startTime time.Time, err error) {
	if u.red == nil {
		return
	}

	u.red.IncRequests(source, service, method)
	u.red.MeasureDuration(source, service, method, startTime)
	if err != nil {
		u.red.IncErrors(source, service, method)
		// TODO Log with sampling
	}
}
