package bolt

import (
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/suite"

	"embedded-db/internal/pkg/configs"
	"embedded-db/internal/pkg/db"
	"embedded-db/internal/pkg/repositories/entities"
)

type UserRepoSuite struct {
	suite.Suite
	db             *db.Bolt
	testDBFilePath string
	repo           *UserRepo
}

func TestUserRepoSuite(t *testing.T) {
	suite.Run(t, new(UserRepoSuite))
}

func (s *UserRepoSuite) SetupSuite() {
	var err error
	s.testDBFilePath = "./testdata/test.db"

	boltDB, err := db.NewBoltDB(configs.Bolt{Path: s.testDBFilePath})
	s.Require().NoError(err)
	s.db = boltDB
	s.repo, err = NewUserRepo(s.db)
	s.Require().NoError(err)
}

func (s *UserRepoSuite) TearDownSuite() {
	s.Require().NoError(s.db.Close())
	s.Require().NoError(os.Remove(s.testDBFilePath))
}

func (s *UserRepoSuite) TestSaveBatch() {
	users := make([]entities.User, 1000)
	for i := 0; i < 1000; i++ {
		users[i] = entities.User{ID: strconv.Itoa(i), Rate: float64(i)}
	}
	err := s.repo.SaveBatch(users)
	s.Require().NoError(err)

	user, err := s.repo.GetUser("23")
	s.Require().NoError(err)
	s.Assert().Equal(user.ID, "23")
	s.Assert().Equal(user.Rate, 23.0)
}

func (s *UserRepoSuite) TestGetUser() {
	user, err := s.repo.GetUser("23")
	s.Require().NoError(err)
	s.Assert().Equal(user.ID, "23")
	s.Assert().Equal(user.Rate, 23.0)
}

func BenchmarkUserRepo_Get(b *testing.B) {
	b.ReportAllocs()
	b.StopTimer()

	testDBFilePath := "./testdata/benchmark.test.db"

	boltDB, err := db.NewBoltDB(configs.Bolt{Path: testDBFilePath})
	if err != nil {
		b.Fatal(err)
	}
	defer boltDB.Close()
	defer os.Remove(testDBFilePath)

	repo, err := NewUserRepo(boltDB)
	if err != nil {
		b.Fatal(err)
	}

	users := make([]entities.User, 1000)
	for i := 0; i < 1000; i++ {
		users[i] = entities.User{ID: strconv.Itoa(i), Rate: float64(i)}
	}
	err = repo.SaveBatch(users)
	if err != nil {
		b.Fatal(err)
	}

	b.SetParallelism(1000)
	b.StartTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			if _, err := repo.GetUser("23"); err != nil {
				b.Fatal(err)
			}
		}
	})
	b.StopTimer()
}
