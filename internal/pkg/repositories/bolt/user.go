package bolt

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"github.com/benbjohnson/clock"
	bolt "go.etcd.io/bbolt"

	"embedded-db/internal/pkg/db"
	"embedded-db/internal/pkg/metrics"
	"embedded-db/internal/pkg/repositories"
	"embedded-db/internal/pkg/repositories/entities"
)

const (
	source  = "bolt_db"
	service = "user_repo"
)

type UserRepo struct {
	client *db.Bolt
	time   clock.Clock
	red    metrics.REDMetrics
	bucket []byte
}

var _ repositories.UserRepo = (*UserRepo)(nil)

type Option func(repo *UserRepo)

func WithREDMetric(redMetrics metrics.REDMetrics) Option {
	return func(repo *UserRepo) {
		repo.red = redMetrics
	}
}

func NewUserRepo(client *db.Bolt, opts ...Option) (*UserRepo, error) {
	ur := &UserRepo{
		client: client,
		bucket: []byte("users"),
		time:   clock.New(),
	}

	for _, opt := range opts {
		opt(ur)
	}

	err := ur.initializeBucket()
	if err != nil {
		return nil, err
	}
	return ur, nil
}

func (u *UserRepo) GetUser(id string) (user entities.User, err error) {
	startTime, methodName := u.time.Now(), "get_user"
	defer u.updateMetrics(methodName, startTime, err)

	err = u.client.DB.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(u.bucket)
		result := bucket.Get([]byte(id))
		return json.Unmarshal(result, &user)
	})
	if err != nil {
		return entities.User{}, err
	}
	return user, nil
}

const packSize = 10_000

// Very slow execution for 1million rows! Around 804 seconds!
func (u *UserRepo) SaveBatch(users []entities.User) (err error) {
	startTime, methodName := u.time.Now(), "save_batch"
	defer u.updateMetrics(methodName, startTime, err)

	keyValuePair := make([][][]byte, len(users))
	for i := range users {
		userByte, _ := json.Marshal(users[i])
		keyValuePair[i] = [][]byte{[]byte(users[i].ID), userByte}
	}

	batchs := len(keyValuePair) / packSize
	if batchs == 0 {
		err = u.client.DB.Update(func(tx *bolt.Tx) error {
			bucket := tx.Bucket(u.bucket)
			for i := range keyValuePair {
				err := bucket.Put(keyValuePair[i][0], keyValuePair[i][1])
				if err != nil {
					return err
				}
			}
			return nil
		})
	} else {
		wg := &sync.WaitGroup{}
		errChan := make(chan error, 1000)
		bStart := u.time.Now()

		for i := 0; i < batchs; i++ {
			start := i * packSize
			end := start + packSize
			pack := keyValuePair[start:end]

			wg.Add(1)
			go u.batchWrite(i, wg, pack, errChan)
		}

		wg.Wait()
		close(errChan)
		fmt.Println("Batch write finished: ", time.Since(bStart).Seconds())
		return <-errChan
	}

	return err
}

func (u *UserRepo) batchWrite(batchN int, wg *sync.WaitGroup, keyValuePair [][][]byte, errCh chan error) {
	defer wg.Done()

	err := u.client.DB.Batch(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(u.bucket)
		for i := range keyValuePair {
			err := bucket.Put(keyValuePair[i][0], keyValuePair[i][1])
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		select {
		case errCh <- err:
		default:
		}
	}
}

func (u *UserRepo) initializeBucket() (err error) {
	err = u.client.DB.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(u.bucket)
		return err
	})
	return err
}

func (u *UserRepo) updateMetrics(method string, startTime time.Time, err error) {
	if u.red == nil {
		return
	}

	u.red.IncRequests(source, service, method)
	u.red.MeasureDuration(source, service, method, startTime)
	if err != nil {
		u.red.IncErrors(source, service, method)
		// TODO Log with sampling
	}
}
