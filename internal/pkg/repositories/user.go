package repositories

import "embedded-db/internal/pkg/repositories/entities"

type UserRepo interface {
	GetUser(id string) (entities.User, error)
	SaveBatch(users []entities.User) error
}
