package badger

import (
	"encoding/json"
	"time"

	"github.com/benbjohnson/clock"
	"github.com/dgraph-io/badger/v3"

	"embedded-db/internal/pkg/db"
	"embedded-db/internal/pkg/metrics"
	"embedded-db/internal/pkg/repositories"
	"embedded-db/internal/pkg/repositories/entities"
)

const (
	source  = "badger_db"
	service = "user_repo"
)

type UserRepo struct {
	client *db.Badger
	time   clock.Clock
	red    metrics.REDMetrics
	bucket []byte
}

var _ repositories.UserRepo = (*UserRepo)(nil)

type Option func(repo *UserRepo)

func WithREDMetric(redMetrics metrics.REDMetrics) Option {
	return func(repo *UserRepo) {
		repo.red = redMetrics
	}
}

func NewUserRepo(client *db.Badger, opts ...Option) (*UserRepo, error) {
	ur := &UserRepo{
		client: client,
		time:   clock.New(),
	}
	for _, opt := range opts {
		opt(ur)
	}
	return ur, nil
}

func (u *UserRepo) GetUser(id string) (user entities.User, err error) {
	startTime, methodName := u.time.Now(), "get_user"
	defer u.updateMetrics(methodName, startTime, err)

	err = u.client.DB.View(func(txn *badger.Txn) error {
		result, err := txn.Get([]byte(id))
		if err != nil {
			return err
		}
		return json.Unmarshal(result.Key(), &user)
	})
	if err != nil {
		return entities.User{}, err
	}
	return user, nil
}

const packSize = 10_000

func (u *UserRepo) SaveBatch(users []entities.User) (err error) {
	startTime, methodName := u.time.Now(), "save_batch"
	defer u.updateMetrics(methodName, startTime, err)

	keyValuePair := make([][][]byte, len(users))
	for i := range users {
		userByte, _ := json.Marshal(users[i])
		keyValuePair[i] = [][]byte{[]byte(users[i].ID), userByte}
	}

	err = u.client.DB.Update(func(txn *badger.Txn) error {
		for i := range keyValuePair {
			err := txn.Set(keyValuePair[i][0], keyValuePair[i][1])
			if err != nil {
				return err
			}
		}
		return nil
	})
	txn := u.client.DB.NewTransaction(true)
	for i := range keyValuePair {
		key, value := keyValuePair[i][0], keyValuePair[i][1]
		if err := txn.Set(key, value); err == badger.ErrTxnTooBig {
			if err = txn.Commit(); err != nil {
				return err
			}
			txn = u.client.DB.NewTransaction(true)
			if err = txn.Set(key, value); err != nil {
				return err
			}
		}
	}
	if err = txn.Commit(); err != nil {
		return err
	}
	return err
}

func (u *UserRepo) updateMetrics(method string, startTime time.Time, err error) {
	if u.red == nil {
		return
	}

	u.red.IncRequests(source, service, method)
	u.red.MeasureDuration(source, service, method, startTime)
	if err != nil {
		u.red.IncErrors(source, service, method)
		// TODO Log with sampling
	}
}
