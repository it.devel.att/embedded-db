package db

import (
	badger "github.com/dgraph-io/badger/v3"

	"embedded-db/internal/pkg/configs"
)

type Badger struct {
	DB *badger.DB
}

func NewBadgerDB(cfg configs.Badger) (*Badger, error) {
	db, err := badger.Open(badger.DefaultOptions(cfg.Path))
	if err != nil {
		return nil, err
	}
	return &Badger{db}, nil
}

func (b *Badger) Close() error {
	return b.DB.Close()
}
