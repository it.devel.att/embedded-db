package db

import (
	bolt "go.etcd.io/bbolt"

	"embedded-db/internal/pkg/configs"
)

type Bolt struct {
	DB *bolt.DB
}

func NewBoltDB(cfg configs.Bolt) (*Bolt, error) {
	db, err := bolt.Open(cfg.Path, 0666, nil)
	if err != nil {
		return nil, err
	}
	db.MaxBatchSize = 100_000
	return &Bolt{db}, nil
}

func (b *Bolt) Close() error {
	return b.DB.Close()
}
