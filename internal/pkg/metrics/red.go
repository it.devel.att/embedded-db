package metrics

import "time"

var redLabels = []string{"source", "service", "method"}

func REDLabels() []string {
	return redLabels
}

type REDMetrics interface {
	IncRequests(source, service, method string)
	IncErrors(source, service, method string)
	MeasureDuration(source, service, method string, startTime time.Time)
}
