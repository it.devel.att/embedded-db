package prometheus

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"

	"embedded-db/internal/pkg/metrics"
)

type REDMetrics struct {
	requests *prometheus.CounterVec
	errors   *prometheus.CounterVec
	duration *prometheus.SummaryVec
}

var _ metrics.REDMetrics = (*REDMetrics)(nil)

func NewREDMetric() *REDMetrics {
	return &REDMetrics{
		requests: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "request_counter",
				Help: "Amount of request total",
			},
			metrics.REDLabels(),
		),
		errors: promauto.NewCounterVec(
			prometheus.CounterOpts{
				Name: "error_counter",
				Help: "Amount of failed requests",
			},
			metrics.REDLabels(),
		),
		duration: promauto.NewSummaryVec(
			prometheus.SummaryOpts{
				Name:       "request_duration",
				Help:       "Request duration",
				Objectives: map[float64]float64{0.5: 0.05, 0.95: 0.01, 0.99: 0.001},
			},
			metrics.REDLabels(),
		),
	}
}

func (r *REDMetrics) IncRequests(source, service, method string) {
	r.requests.WithLabelValues(source, service, method).Inc()
}

func (r *REDMetrics) IncErrors(source, service, method string) {
	r.errors.WithLabelValues(source, service, method).Inc()
}

func (r *REDMetrics) MeasureDuration(source, service, method string, startTime time.Time) {
	r.duration.WithLabelValues(source, service, method).Observe(float64(time.Since(startTime).Nanoseconds()))
}
