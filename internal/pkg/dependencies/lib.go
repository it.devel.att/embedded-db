package dependencies

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"

	"embedded-db/internal/pkg/configs"
	"embedded-db/internal/pkg/db"
	"embedded-db/internal/pkg/handlers"
	metricProm "embedded-db/internal/pkg/metrics/prometheus"
	"embedded-db/internal/pkg/repositories"
	repoBadger "embedded-db/internal/pkg/repositories/badger"
	repoBolt "embedded-db/internal/pkg/repositories/bolt"
	repoMemory "embedded-db/internal/pkg/repositories/memory"
	"embedded-db/internal/pkg/servers"
)

type Dependencies interface {
	LoadENV()
	Config() *configs.Config
	Bolt() *db.Bolt
	Badger() *db.Badger

	BoltUserRepo() *repoBolt.UserRepo
	BadgerUserRepo() *repoBadger.UserRepo
	MemoryUserRepo() *repoMemory.UserRepo

	PromREDMetrics() *metricProm.REDMetrics

	PromHTTPHandler() http.Handler
	UserHandler() *handlers.UserHandler
	UserHandlerWithRepo(repo repositories.UserRepo) *handlers.UserHandler

	MetricServer() *servers.BaseServer
	AppServer() *servers.BaseServer
	AppServerWithHandler(handler http.Handler) *servers.BaseServer

	WaitForStop()
}

type dep struct {
	log *zap.Logger
	cfg *configs.Config

	promREDMetrics *metricProm.REDMetrics

	bolt           *db.Bolt
	badger         *db.Badger
	boltUserRepo   *repoBolt.UserRepo
	badgerUserRepo *repoBadger.UserRepo
	memoryUserRepo *repoMemory.UserRepo

	userHandler     *handlers.UserHandler
	promHTTPHandler http.Handler

	appServer    *servers.BaseServer
	metricServer *servers.BaseServer

	closeCallbacks []func() error
}

func NewDependencies() Dependencies {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	return &dep{log: logger}
}

func (d *dep) LoadENV() {
	if err := godotenv.Load(); err != nil {
		d.log.Error("Err load .env file", zap.Error(err))
	}
}

func (d *dep) Config() *configs.Config {
	if d.cfg == nil {
		var err error
		d.cfg, err = configs.NewConfig()
		if err != nil {
			d.log.Panic("", zap.Error(err))
		}
		d.log.Info("", zap.String("config", fmt.Sprintf("%+v", d.cfg)))
	}
	return d.cfg
}

func (d *dep) PromREDMetrics() *metricProm.REDMetrics {
	if d.promREDMetrics == nil {
		d.promREDMetrics = metricProm.NewREDMetric()
		d.successLog("promREDMetrics")
	}
	return d.promREDMetrics
}

func (d *dep) Bolt() *db.Bolt {
	if d.bolt == nil {
		var err error
		d.bolt, err = db.NewBoltDB(d.Config().Bolt)
		if err != nil {
			d.log.Panic("", zap.Error(err))
		}
		d.addCloseCB(d.bolt.Close)
		d.successLog("boltDB")
	}
	return d.bolt
}

func (d *dep) Badger() *db.Badger {
	if d.badger == nil {
		var err error
		d.badger, err = db.NewBadgerDB(d.Config().Badger)
		if err != nil {
			d.log.Panic("", zap.Error(err))
		}
		d.addCloseCB(d.badger.Close)
		d.successLog("badgerDB")
	}
	return d.badger
}

func (d *dep) BoltUserRepo() *repoBolt.UserRepo {
	if d.boltUserRepo == nil {
		var err error
		d.boltUserRepo, err = repoBolt.NewUserRepo(d.Bolt(), repoBolt.WithREDMetric(d.PromREDMetrics()))
		if err != nil {
			d.log.Panic("", zap.Error(err))
		}
		d.successLog("boltUserRepo")
	}
	return d.boltUserRepo
}

func (d *dep) BadgerUserRepo() *repoBadger.UserRepo {
	if d.badgerUserRepo == nil {
		var err error
		d.badgerUserRepo, err = repoBadger.NewUserRepo(d.Badger(), repoBadger.WithREDMetric(d.PromREDMetrics()))
		if err != nil {
			d.log.Panic("", zap.Error(err))
		}
		d.successLog("badgerUserRepo")
	}
	return d.badgerUserRepo
}

func (d *dep) MemoryUserRepo() *repoMemory.UserRepo {
	if d.memoryUserRepo == nil {
		var err error
		d.memoryUserRepo, err = repoMemory.NewUserRepo(repoMemory.WithREDMetric(d.PromREDMetrics()))
		if err != nil {
			d.log.Panic("", zap.Error(err))
		}
		d.successLog("memoryUserRepo")
	}
	return d.memoryUserRepo
}

func (d *dep) UserHandler() *handlers.UserHandler {
	if d.userHandler == nil {
		d.userHandler = handlers.NewUserHandler(d.log, d.BoltUserRepo())
		d.successLog("userHandler")
	}
	return d.userHandler
}

func (d *dep) UserHandlerWithRepo(repo repositories.UserRepo) *handlers.UserHandler {
	if d.userHandler == nil {
		d.userHandler = handlers.NewUserHandler(d.log, repo)
		d.successLog("userHandler")
	}
	return d.userHandler
}

func (d *dep) PromHTTPHandler() http.Handler {
	if d.promHTTPHandler == nil {
		mux := http.NewServeMux()
		mux.Handle("/metrics", promhttp.Handler())
		d.promHTTPHandler = mux
		d.successLog("promHTTPHandler")
	}
	return d.promHTTPHandler
}

func (d *dep) MetricServer() *servers.BaseServer {
	if d.metricServer == nil {
		d.metricServer = servers.NewServer(d.log, d.Config().Metric, d.PromHTTPHandler())
		d.addCloseCB(d.metricServer.Stop)
		d.successLog("metricServer")
	}
	return d.metricServer
}

func (d *dep) AppServer() *servers.BaseServer {
	if d.appServer == nil {
		d.appServer = servers.NewServer(d.log, d.Config().App, d.UserHandler())
		d.addCloseCB(d.appServer.Stop)
		d.successLog("appServer")
	}
	return d.appServer
}

func (d *dep) AppServerWithHandler(handler http.Handler) *servers.BaseServer {
	if d.appServer == nil {
		d.appServer = servers.NewServer(d.log, d.Config().App, handler)
		d.addCloseCB(d.appServer.Stop)
		d.successLog("appServer")
	}
	return d.appServer
}

func (d *dep) WaitForStop() {
	sigCH := make(chan os.Signal)
	signal.Notify(sigCH, syscall.SIGINT, syscall.SIGTERM)
	<-sigCH
	d.log.Info("Received interrupt signal")
	d.callCloseCBs()
}

func (d *dep) successLog(component string) {
	d.log.Info("Success", zap.String("component", component))
}

func (d *dep) addCloseCB(cb func() error) {
	d.closeCallbacks = append(d.closeCallbacks, cb)
}

func (d *dep) callCloseCBs() {
	for i := len(d.closeCallbacks) - 1; i >= 0; i-- {
		if err := d.closeCallbacks[i](); err != nil {
			d.log.Error("", zap.Error(err))
		}
	}
	_ = d.log.Sync()
}
