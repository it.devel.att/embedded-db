package configs

import (
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	Metric Server `mapstructure:"metric"`
	App    Server `mapstructure:"app"`
	Bolt   Bolt   `mapstructure:"bolt"`
	Badger Badger `mapstructure:"badger"`
}

type Bolt struct {
	Path string `mapstructure:"path"`
}

type Badger struct {
	Path string `mapstructure:"path"`
}

type Server struct {
	Port string `mapstructure:"port"`
}

func NewConfig() (*Config, error) {
	config := &Config{}

	viper.SetDefault("bolt.path", "/tmp/random.bolt.db")
	viper.SetDefault("badger.path", "/tmp/random.badger.db")
	viper.SetDefault("app.port", "8080")
	viper.SetDefault("metric.port", "3000")

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	err := viper.Unmarshal(config)
	return config, err
}
