package main

import (
	"strconv"

	"embedded-db/internal/pkg/dependencies"
	"embedded-db/internal/pkg/repositories/entities"
)

const usersCount = 1_000_000

func main() {
	deps := dependencies.NewDependencies()
	deps.LoadENV()

	// Preload test data
	repo := deps.BadgerUserRepo()
	deps.MetricServer().Run()

	users := make([]entities.User, usersCount)
	for i := 0; i < usersCount; i++ {
		users[i] = entities.User{ID: strconv.Itoa(i), Rate: float64(i)}
	}
	if err := repo.SaveBatch(users); err != nil {
		panic(err)
	}
	// Run servers
	deps.AppServerWithHandler(deps.UserHandlerWithRepo(repo)).Run()
	deps.WaitForStop()
}
