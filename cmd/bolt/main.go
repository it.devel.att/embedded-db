package main

import (
	"embedded-db/internal/pkg/dependencies"
)

const usersCount = 1_000_000

func main() {
	deps := dependencies.NewDependencies()
	deps.LoadENV()

	// Preload test data
	repo := deps.BoltUserRepo()
	deps.MetricServer().Run()

	// TODO For BOLT DB it's to heavy operation around 800 seconds, run only for first start
	//users := make([]entities.User, usersCount)
	//for i := 0; i < usersCount; i++ {
	//	users[i] = entities.User{ID: strconv.Itoa(i), Rate: float64(i)}
	//}
	//if err := repo.SaveBatch(users); err != nil {
	//	panic(err)
	//}
	// Run servers
	deps.AppServerWithHandler(deps.UserHandlerWithRepo(repo)).Run()
	deps.WaitForStop()
}
