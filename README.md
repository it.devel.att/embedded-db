# TODO Describe pros and cons

Fast desc:

Bolt is very fast for reading, but very heavy for load big amount of data! (800seconds)

Badger is very fast for load big set of rows (1million) (2s) but not very memory efficient in reading data (and a little slower than bolt)

// TODO Screenshots and so on

TODO Future solution for using bolt for example

1. Upload file to s3
2. One service read s3 file and create from scratch bolt.db file by file from s3
3. Download new bolt.db from s3 and recreate db in memory

### Tests
##### Struct for tests
```golang
type User struct {
	ID   string  `json:"id"`
	Rate float64 `json:"rate"`
}
```
```golang
for i := 0; i < 1_000_000; i++ {
    users[i] = entities.User{ID: strconv.Itoa(i), Rate: float64(i)}
}
```
> For bolt, I use preloaded bolt.db file because every time load 1m rows in to bolt it's to long
```shell script
wrk -t12 -c1000 -d10m http://localhost:8080/user
```
#### Bolt
```
Running 10m test @ http://localhost:8080/user
  12 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    13.82ms   13.59ms 236.66ms   81.80%
    Req/Sec     7.17k     1.91k   33.45k    72.33%
  51319136 requests in 10.00m, 3.58GB read
Requests/sec:  85517.81
Transfer/sec:      6.12MB
```
##### Bolt repository metrics
![](docs/imgs/bolt/bolt_repo_metrics_1.png)

##### Bolt go metrics
![](docs/imgs/bolt/bolt_go_metrics_1.png)

##### Bolt io metrics
![](docs/imgs/bolt/bolt_io_metrics_1.png)
##### Final db file size
115M
> I use simple json.Marshall and in fact store json in bolt (it's not very efficient way indeed)

#### Memory (sync.Map)
```shell script
wrk -t12 -c1000 -d10m http://localhost:8080/user
```
```
Running 10m test @ http://localhost:8080/user
  12 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    12.25ms   14.91ms 379.47ms   90.84%
    Req/Sec     8.80k     3.32k   51.90k    71.24%
  62939820 requests in 10.00m, 4.40GB read
Requests/sec: 104882.24
Transfer/sec:      7.50MB
```
##### Memory repository metrics
![](docs/imgs/memory/repo_metrics_1.png)

##### Memory go metrics
![](docs/imgs/memory/go_metrics_1.png)

> Very fast but consume more RAM

#### Badger
> On the very first load of 1m rows to badger it consume a lot disk memory, I see in badger dir on first run this
```console
$ ls -lh
total 21M
-rw-rw-r-- 1 yaroslav yaroslav  18M may 27 21:00 000001.sst
-rw-rw-r-- 1 yaroslav yaroslav 2,0G may 27 21:00 000001.vlog
-rw-rw-r-- 1 yaroslav yaroslav 128M may 27 21:00 00002.mem
-rw-rw-r-- 1 yaroslav yaroslav 1,0M may 27 21:00 DISCARD
-rw------- 1 yaroslav yaroslav   28 may 27 21:00 KEYREGISTRY
-rw-rw-r-- 1 yaroslav yaroslav    6 may 27 21:00 LOCK
-rw------- 1 yaroslav yaroslav   30 may 27 21:00 MANIFEST
```

But after stop program and close budger db client it will be:
```console
$ ls -lh
total 19M
-rw-rw-r-- 1 yaroslav yaroslav  18M may 27 21:00 000001.sst
-rw-rw-r-- 1 yaroslav yaroslav   20 may 27 21:05 000001.vlog
-rw-rw-r-- 1 yaroslav yaroslav 1,1M may 27 21:05 000002.sst
-rw-rw-r-- 1 yaroslav yaroslav 1,0M may 27 21:00 DISCARD
-rw------- 1 yaroslav yaroslav   28 may 27 21:00 KEYREGISTRY
-rw------- 1 yaroslav yaroslav   44 may 27 21:05 MANIFEST

``` 
> TODO Need to read more about badger and how he store files on disk

```console
$ wrk -t12 -c1000 -d10m http://localhost:8080/user
Running 10m test @ http://localhost:8080/user
  12 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    14.69ms   14.80ms 213.18ms   85.28%
    Req/Sec     6.81k     1.58k   33.85k    72.91%
  48707051 requests in 10.00m, 4.26GB read
  Non-2xx or 3xx responses: 48707051
Requests/sec:  81165.23
Transfer/sec:      7.28MB
```
##### Badger repository metrics
![](docs/imgs/badger/repo_metrics_1.png)

##### Badger go metrics
![](docs/imgs/badger/go_metrics_1.png)

##### Badger io metrics
![](docs/imgs/badger/io_metrics_1.png)
